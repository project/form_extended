<?php

function _form_extended_rules_forms_rules_action_info() {
  _form_extended_included_handlers();
  $handlers = form_extended_get_handlers();
  $actions = array();
  foreach ($handlers as $handler) {
    $default_class = $handler['default_class'];
    $handler['class_singelton']  = isset($handler['class_singelton']) ? $handler['class_singelton'] : new $default_class();
    if (method_exists($handler['class_singelton'], 'getRulesActionInfo')) {
      $actions += $handler['class_singelton']->getRulesActionInfo();
    }
  }
  return $actions;
}