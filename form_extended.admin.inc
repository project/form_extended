<?php
/**
 * Admin settings callback.
 */
function form_extended_settings() {
  // Charsets
  $form['includes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Included Handlers'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['includes']['form_extended_include_inline_label_handler'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Inline Label Handler'),
    '#default_value' => variable_get('form_extended_include_inline_label_handler',FALSE),
  );
  $form['includes']['form_extended_include_autoresize'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Auto-resize Handler'),
    '#default_value' => variable_get('form_extended_include_autoresize',FALSE),
  );
  return system_settings_form($form);
}