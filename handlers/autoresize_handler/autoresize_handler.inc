<?php
/**
 * @file
 * InlineFieldLabel JQuery plugin included handler
 */

interface AutoresizeHandlerInterface extends FormHandlerInterface {
}

class AutoresizeHandlerDefault implements AutoresizeHandlerInterface {
  private static $instanceCounter = 0;
  public function __construct() {
    self::$instanceCounter++;
    if (self::$instanceCounter == 1) {
     $jq_plugin = drupal_get_path('module', 'form_extended').'/jquery_plugins/elastic/jquery.elastic.js';
     drupal_add_js($jq_plugin, 'module');
    }
  }
  public function callback_method($options = array()) {
    if (self::$instanceCounter == 1) {
      $class = str_replace('#', '_', $options['form_property']);
      $this->autoresize_callback($class);
    }
  }
  public function element_defenition(&$elements, $data = array()) {
    //current implementation doesn't support CCK fields;
    if ($elements['#type'] == 'textarea' && $data['#autoresize_handler']) {
      $classes = '_autoresize_handler';
      if (isset($elements['#attributes']['class'])) {
        $elements['#attributes']['class'] .= ' '. $classes; 
      }
      else {
         $elements['#attributes']['class'] = $classes; 
      }
      
    }
  }
  protected function autoresize_callback($class) {
    $js_setting = array('autoresize_handler' => array('element' => '.'.$class));
    $js_file = drupal_get_path('module', 'form_extended').'/handlers/autoresize_handler/autoresize_handler.js';
    drupal_add_js($js_file, 'module');
    drupal_add_js($js_setting, 'setting');
  }
  public function getRulesActionInfo() {
    return array(
        'form_extended_autoresize_action' => array(
          'label' => t('Make text area auto resize itself'),
          'arguments' => array(
            'form' => array('type' => 'form', 'label' => t('Form')),
            'element' => array(
              'type' => 'string',
              'label' => t('Form element ID'),
              'description' => t('The element that should be targeted.') .' '. _rules_forms_element_description(),
            ),
            'value' => array(
              'type' => 'string',
              'label' => t('Number of Rows'),
              'description' => t('Insert the Number of default Rows')
            ),
            
          ),
          'module' => 'Rules Forms',
        ),
      );
  }
}

function form_extended_autoresize_action(&$form, $element, $value) {
  $form_element = &_rules_forms_get_element($form, $element);
  if (empty($form_element)) {
    return;
  }
  // Check if the form element is a CCK field
  if (array_key_exists('#field_name', $form_element)) {
    // Check if multiple default values are allowed
    if (isset($form_element['#inline_field_label_handler'][0])) {
      foreach ($form_element['#inline_field_label_handler'] as $i => $val) {
        $form_element['#autoresize_handler'][$i]['value'] = TRUE;
        $form_element['#resizable'][$i]['value'] = FALSE;
        $form_element['#rows'][$i]['value'] = $value;
        
      }
    }
    else {
      $form_element['#autoresize_handler']['value'] = TRUE;
      $form_element['#resizable']['value'] = FALSE;
      $form_element['#rows']['value'] = $value;
    }
  }
  else {
    $form_element['#autoresize_handler'] = TRUE;
    $form_element['#resizable'] = FALSE;
    $form_element['#rows'] = $value;
  }
}