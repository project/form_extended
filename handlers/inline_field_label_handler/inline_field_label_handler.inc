<?php
/**
 * @file
 * InlineFieldLabel JQuery plugin included handler
 */

interface InlineFieldLabelInterface extends FormHandlerInterface {
}

class InlineFieldLabelDefault implements InlineFieldLabelInterface {
  private static $instanceCounter = 0;
  public function __construct() {
    self::$instanceCounter++;
    if (self::$instanceCounter == 1) {
      $jq_plugin = drupal_get_path('module', 'form_extended').'/jquery_plugins/metadata/jquery.metadata.js';
      drupal_add_js($jq_plugin, 'module');
      
      $jq_plugin = drupal_get_path('module', 'form_extended').'/jquery_plugins/inlineFieldLabel/jquery.inlineFieldLabel.js';
      drupal_add_js($jq_plugin, 'module');
      
    }
  }
  public function callback_method($options = array()) {
    if (self::$instanceCounter == 1) {
      $class = str_replace('#', '_', $options['form_property']);
      $this->inlineFieldLabel_callback($class);
    }
  }
  public function element_defenition(&$elements, $data = array()) {
    //current implementation don't support CCK fields;
    if (!isset($elements['#type'])) return;
    switch ($elements['#type']) {
      case 'textarea':
      case 'textfield':
      case 'password':
        $attribute = 'class';
        break;
      default:
        return;
        break;
    }
    $json = array();
    //flaten the arrays
    foreach ($data as $data_elements) {
      foreach ($data_elements as $key => $data_element) {
        $json[$key] = $data_element;
      }   
    }
    $json_str = str_replace('"', '\'', drupal_to_js($json));
    if (isset($elements['#attributes'][$attribute])) {
      $elements['#attributes'][$attribute] .=   ' '.  $json_str;
    }
    else {
      $elements['#attributes'][$attribute] = $json_str;
    }
    $classes = str_replace('#', '_', implode(' ', array_keys($data)));
    $elements['#attributes']['class'] .= ' '. $classes;
  }
  protected function inlineFieldLabel_callback($class) {
    $js_setting = array('inline_fieldLabel_handler' => array('element' => '.'.$class));
    $js_file = drupal_get_path('module', 'form_extended').'/handlers/inline_field_label_handler/inline_field_label_handler.js';
    drupal_add_js($js_file, 'module');
    drupal_add_js($js_setting, 'setting');
  }
  public function getRulesActionInfo() {
    return array(
        'form_extended_inline_field_label_action' => array(
          'label' => t('Set inline label to form element'),
          'arguments' => array(
            'form' => array('type' => 'form', 'label' => t('Form')),
            'element' => array(
              'type' => 'string',
              'label' => t('Form element ID'),
              'description' => t('The element that should be targeted.') .' '. _rules_forms_element_description(),
            ),
            'value' => array(
              'type' => 'string',
              'label' => t('Label'),
              'description' => t('The value(s) that will be displayed as default. If the form element allows multiple values, enter one value per line.'),
            ),
          ),
          'module' => 'Rules Forms',
        ),
      );
  }
}

//Rules Callbacks - the callback are out of the class becuse rules forms looks for function_exists and not class method


/**
 * Action implementation: set inline label to form element
 */
function form_extended_inline_field_label_action(&$form, $element, $value) {
  
  $form_element = &_rules_forms_get_element($form, $element);
  if (empty($form_element)) {
    return;
  }
  $lines = explode("\r\n", $value);
  // Check if the form element is a CCK field
  if (array_key_exists('#field_name', $form_element)) {
    // Check if multiple default values are allowed
    if (isset($form_element['#inline_field_label_handler'][0])) {
      
      // Unset old default values
      $form_element['#inline_field_label_handler'] = array();
      foreach ($lines as $i => $line) {
        $form_element['#inline_field_label_handler'][$i]['value']['label'] = $line;
      }
    }
    else {
      dpm($form_element);
      $form_element['#inline_field_label_handler']['label'] = $value;
    }
  }
  else {
    if (count($lines) == 1) {
      $form_element['#inline_field_label_handler']['label'] = $value;
    }
    else {
      $form_element['#inline_field_label_handler']['label'] = $lines;
    }
  }
}

